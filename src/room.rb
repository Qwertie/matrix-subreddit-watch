require 'set'
require 'time'
require './src/reddit_helper'

class Room
	attr_reader :last_run_time

	def initialize
		@subreddits = Set.new
		@last_run_time = Time.now.utc
	end

	def add(subreddit)
		@subreddits << subreddit
	end

	def delete(subreddit)
		@subreddits.delete subreddit
	end

	def list
		@subreddits
	end

	def new_posts
		@last_run_time = Time.now.utc
		RedditHelper.new_posts(self)
	end
end
