require 'httparty'
require 'json'

class MatrixOrg
	attr_accessor :room_list
	def initialize(username, password)
		raise "Login type not supported." unless self.check_supported_login
		@access_token = self.login(username, password)
		@room_list = self.get_room_list
	end

	def login(username, password)
		data = JSON.parse(
			HTTParty.post(
				"#{Config['matrix_url']}/_matrix/client/r0/login",
				:body => {
					:type => 'm.login.password',
					:user => username,
					:password => password
				}.to_json,
				:headers => { 'Content-Type' => 'application/json' }
			).body, symbolize_names: true
		)
		return data[:access_token]
	end

	def check_supported_login
		res = HTTParty.get("#{Config['matrix_url']}/_matrix/client/r0/login").body
		login_type = JSON(
			res,
			:symbolize_names => true
		)[:flows].first[:type] # TODO: check all login types

		return login_type == "m.login.password"
	end

	def get_room_list
		data = JSON.parse(
			HTTParty.get(
				"https://matrix.org/_matrix/client/r0/sync?access_token=#{@access_token}"
			).body,
			symbolize_names: true)
		return data[:rooms][:join].keys
	end
end
