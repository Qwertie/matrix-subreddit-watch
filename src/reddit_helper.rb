require 'open-uri'
require 'simple-rss'
require 'time'

module RedditHelper
	def RedditHelper.new_posts(room)
		posts = []
		room.list.each do |subreddit|
			self.fetch_posts(subreddit).each do |post|
				posts << post if post[:updated] > room.last_run_time
			end
		end

		puts posts.map(&:title)
		puts posts.map(&:updated)

	end

	def RedditHelper.fetch_posts(subreddit)
		SimpleRSS.parse(open("https://reddit.com/r/#{subreddit}/new.rss")).entries
	end
end
